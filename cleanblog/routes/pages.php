<?php



/*Route::get('pages/{id}/{slug}', 'PagesController@show', function ($id) {
  return 'Page '.$id;
})->name('pages');*/

Route::prefix('pages')->group(function () {
  Route::get('/{id}/{slug}', 'PagesController@show')
    ->name('pages')
    ->where('id', '[0-9]+', 'name', '[a-z]+');

  Route::get('/add', 'PagesController@add')
    ->name('addPages');
});

/*Route::prefix('pages')->group(function () {
  Route::get('/{id}/{slug}', 'PagesController@show')
    ->name('pages')
    ->where('id', '[0-9]+', 'name', '[a-z]+');

  Route::get('/add', 'PagesController@add')
    ->name('addPages');
});*/
