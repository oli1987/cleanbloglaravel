<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
|--------------------------------------------------------------------------
|MISE A DISPOSITION DE DONNEES POUR CERTAINES VUES
|--------------------------------------------------------------------------
*/

View::composer('pages.menu', function ($view) {
    $view->with('pages', App\Http\Models\Page::all());
});

/*
|--------------------------------------------------------------------------
|ROUTES
|--------------------------------------------------------------------------
*/

Route::get('/', 'PagesController@show')->name('homepage');

require base_path('routes/pages.php');
