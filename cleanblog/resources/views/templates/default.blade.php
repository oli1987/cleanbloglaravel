{{--
  ./resources/templates/default.blade.php
  Template par défaut
 --}}
<!DOCTYPE html>
<html lang="en">

  <head>
    @include('templates.partials.head')
  </head>

  <body>

    <!-- Navigation -->
    @include('templates.partials.nav')

    <!-- CONTENU -->
    <main>
        @section('content1')
        @show
    </main>

    <!-- Footer -->
    <footer>
      @include('templates.partials.footer')
    </footer>

    <!-- Scripts -->
      @include('templates.partials.scripts')

  </body>

</html>
