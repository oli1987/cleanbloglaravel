{{--
  ./resources/views/pages/menu.blade.php
  Variables disponibles
      - $pages ARRAY(ARRAY(id, titre, ...))
 --}}

<ul class="navbar-nav ml-auto">
  @foreach ($pages as $page)
  <li class="nav-item">
    <a class="nav-link" href="{{ URL::Route('pages.show', $page->id) }}">{{ $page->titreMenu }}</a>
  </li>
  @endforeach
</ul>
