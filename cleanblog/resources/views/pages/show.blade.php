{{--
  ./resources/views/pages/show.blade.php
  Variables disponibles
      - $page(id, titre, sousTitre, titreMenu, texte, image)
 --}}
@extends('templates/default')

@section('title')
  {{ $page->titre }}
@stop

@section('content1')
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('{{ asset('img/'.$page->image) }}')">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>{{ $page->titre }}</h1>
            <span class="subheading">{{ $page->sousTitre }}</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Textes -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="clearfix">
          {!! html_entity_decode($page->texte) !!}
        </div>
        {{-- CHARGEMENT DES CONTENUS COMPLEMENTAIRES --}}
      

      </div>
    </div>
  </div>

  <hr>
@stop
