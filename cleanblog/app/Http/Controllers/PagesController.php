<?php
       namespace App\Http\Controllers;
       use App\Http\Models\Page as PagesMdl;
       use Illuminate\Support\Facades\View;

       class PagesController extends Controller {

         /**
          * [Détails de la page $id]
          * @param integer $id [Id de la page à afficher]
          * @return View       [Vue pages/show.blade.php]
          */

         public function show($id = 1){    // id = par défaut

            // recherche dans la DB
            $page = PagesMdl::find($id);

            // Retourne la vue show
            return View::make('pages.show', ['page' => $page ]);
         }
       }
